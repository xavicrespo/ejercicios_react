import React from 'react';
import "./css/cabecera.css";


function Cabecera(props){

    let estilosCabecera = {
        backgroundColor: props.color
    };

    return (
        <div className="cabecera" style={estilosCabecera} >{props.titulo}</div>
    ); 
}

export default Cabecera;