import React from 'react';
import Bola from './Bola';

function TituloRojo(props) {
    return (
        <h1 style={{ color: "red" }}>{props.children}</h1>
    );
}

function Contenido(props) {
    return (
        <>
            <TituloRojo>Lorem, ipsum dolor.</TituloRojo>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore quo, corporis magni odit sit, aspernatur quos provident ipsa excepturi nulla soluta adipisci officia quod minus ipsum hic, porro dolorem repellat.</p>

            <h1>Capítulo 2</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam veniam laudantium optio consectetur dolores placeat, earum nemo temporibus vitae incidunt obcaecati qui sed omnis. Molestiae voluptatibus facilis ipsa delectus animi.</p>

            <Bola/>
        </>
    );
}

export default Contenido;