import React from 'react';

export default class Flapicon extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            on: this.props.icon1
        } 
        this.pulsar = this.pulsar.bind(this);
    }

    pulsar() {
        this.setState({
            on: !this.state.on
        })
    }
    render() {
        let estilo = {
            margin: "5px",
            display: "inline-block",
            backgroundColor: "beige",
            textAlign: "center"
        }
        let estiloI = {
            margin:"0px"
        }

        let claseIcono = (this.state.on)
            ? this.props.icon1
            : this.props.icon2;

        return (
            <div style={estilo} onClick={this.pulsar}  >
                <i style={estiloI} className={claseIcono} />
            </div>
        );
    }
}