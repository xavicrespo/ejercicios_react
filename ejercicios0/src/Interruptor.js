import React from 'react';
import './css/interruptor.css';

export default class interruptor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            on: this.props.estadoInicial
        }
        this.pulsar = this.pulsar.bind(this);
    }

    pulsar() {
        this.setState({
            on: !this.state.on
        })
    }

    render() {
        let claseIcono = (this.state.on)
            ? "fa fa-toggle-on"
            : "fa fa-toggle-off";
        
        let estadoIcono = (this.state.on) ? "ACTIVO" : "INACTIVO";

        return (
            <div onClick={this.pulsar}>
                <i className={claseIcono} />
                <p>{estadoIcono}</p>
            </div>
        );
    }
}

