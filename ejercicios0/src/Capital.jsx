import React from 'react';
import './css/capital.css'


function Capital(props) {
/*  let inicial = nombre.substring(0, 1); */
    let inicial = props.nom[0];
    inicial = inicial.toUpperCase();

    let ciudad = props.nom;
    ciudad = ciudad.substring(1).toLowerCase();

    let estilo = {
        color: props.color
    }


    return (
        <div className="capital">
            <h1 style={estilo}>{inicial}</h1>
            <p>{ciudad}</p>
        </div>
    );
}

export default Capital;