import React from 'react';

function Mosca(props) {
    return (
        <div style={{
            color: props.color,
            position: "absolute",
            right: "0px",
            top: "0px",
            margin: "25px",
            fontSize: "40px"
        }}>
        <i class="fa fa-bug" aria-hidden="true"/>
        </div>
    );
}
export default Mosca;