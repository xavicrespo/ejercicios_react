import React from 'react';

function Gato(props) {
    let estiloCaja = {
        margin: "10px 0px",
        display: "inline-block",
        border: "1px solid blue",
        textAlign: "center" 
    }

    let estiloP = {
        margin: "0px"
    }

    let url = "https://placekitten.com/" + props.ancho + "/" + props.alto;
    
    return (
        <div style={estiloCaja}>
            <img src={url} />
            <p style={estiloP}>{props.nombre}</p>
        </div>
    )
}
export default Gato;