import React from 'react';
import './css/bola.css';

function Bola(props) {
    return (
        <div className="bola" style={{
            backgroundColor: props.fondo,
            width: props.talla,
            height: props.talla,
            margin: props.margen
        }}> </div>
    );
}
export default Bola;
