import React from 'react';
import Foto from './Foto';
import Titulo from './Titulo';
import Interruptor from './Interruptor';
import Bola from './Bola';
import Mosca from './Mosca';
import Capital from './Capital';
import Gato from './Gato';
import Flapicon from './Flipicon';


function App() {
  return (
    <>
      <Titulo texto="Pruebas con React" />
      <Foto />
      <Interruptor texto="BOOLEAN" estadoInicial={true} />
      <br />
      <Bola talla="80px" margen="10px" fondo="#ff0000" />
      <br />
      <Mosca color="blue" />
      <br />
      <Capital nom="barcelona" color="red" />
      <br />
      <Gato ancho="200" alto="200" nombre="Garfied" />
      <br />
      <Flapicon icon1="fa fa-thumbs-o-up fa-5x" icon2="fa fa-thumbs-o-down fa-5x" /* estadoInicial={true} *//>
    </>
  );
}
export default App;

