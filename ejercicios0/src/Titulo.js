import React from 'react';

/* const Titulo = (props) => <h1 className="titulo">{props.texto}</h1>;
 */
function Titulo(props) {
    return (
        <>
            <h1 className="titulo">{props.texto}</h1>
        </>
    );
}

export default Titulo;