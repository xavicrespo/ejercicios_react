
import React from 'react';

const Boton = (props) => {
    let textoBoton =  (props.estado) ? "Apagar" : "Encender" ;
    return <button onClick={props.onClick} >{textoBoton}</button>
}

export default Boton;