import React from 'react';
import './css/boton.css';

class Boton extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            iconoVisible: false,
            valor: ''
        }
        this.clicar = this.clicar.bind(this);
    }

    clicar() {
        if (this.state.iconoVisible === false) {
            this.setState({ iconoVisible: true, valor: this.props.turno });
            this.props.cambiaTurno(this.props.orden,this.props.turno);
        }
    }

    render() {

        let circulo = "fa fa-circle-o icono";
        let cruz = "fa fa-times icono";
        /* let clasesIcono = (this.props.turno === "x") ? cruz : circulo; */
        let clasesIcono = (this.state.valor === "x") ? cruz : circulo;

        if (this.state.iconoVisible === false) {
            clasesIcono = clasesIcono + " oculto";
        }

        return (
            <div onClick={this.clicar} className="boton">
                <i className={clasesIcono} aria-hidden="true"></i>
            </div>
        );
    }
}

export default Boton;