import React from 'react';
import { MOTOS } from './motos.js';

export default class ListaMotos extends React.Component {

    constructor(props) {
        super(props);
        this.creaFila = this.creaFila.bind(this);//no es necesario si no se utiliza el this
    }

    creaFila(moto, index) {
        return (
            <tr key={index}>
                <td>{moto.marca}</td>
                <td>{moto.model}</td>
                <td>{moto.eur}</td>
                <td>{moto.km}</td>
            </tr>
        );
    }

    honda(moto) {
        /*         if (moto.marca === "HONDA") {
            return true;
        } else {
            return false;
        }
         */
        return moto.marca === "HONDA";
    }

    comparaKm(moto1, moto2) {
        let x = 0;

        if (moto1.km > moto2.km) {
            x = 1
        } else if (moto1.km < moto2.km){
            x = -1
        } else {
            x = 0;
        }

        return x; //1,0,-1
    }
    comparaPrecio(moto1, moto2) {
        let x = 0;

        if (moto1.eur > moto2.eur) {
            x = 1
        } else if (moto1.eur < moto2.eur){
            x = -1
        } else {
            x = 0;
        }

        return x; //1,0,-1
    }

    render() {
        //culaquier funcion que se pasa a map, le pasa dos parametros (elemento,indice)
        //version funcion externa  // let filas = MOTOS.filter(this.honda).map(this.creaFila);
        /*version con function dentro*/   /* let filas = MOTOS.filter(
                    function (moto) {
                        return moto.marca === "GILERA";
                   })
                   .map(this.creaFila);  */

        let filas = MOTOS
            .filter(moto => moto.marca === "YAMAHA")
            .sort(this.comparaPrecio)
            .map(this.creaFila);
        
        let fila = filas[0];
        let filas10 = filas.splice(0,10);

        return (
            <>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Marca</th>
                            <th>Model</th>
                            <th>Precio</th>
                            <th>Km</th>
                        </tr>
                    </thead>
                    <tbody>
                        {filas10}
                    </tbody>
                </table>
            </>
        )
    }
}
