import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText,Container } from 'reactstrap';

export default class Login extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: "",
            errorPassword: false
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleInputChange(e) {
        const target = e.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        //const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        //alert('Your favorite flavor is: ' + this.state.value);
        event.preventDefault();
        if (this.state.password.length > 8) {
            this.setState({errorPassword : true})
        }
    }


    render() {
        let mensajeErrorPass = null;
        if (this.state.errorPassword === true) {
            mensajeErrorPass = <h2>Error! Password incorrecto</h2>;
        }
        return (
            <Container>
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label for="campoEmail">Email</Label>
                        <Input onChange={this.handleInputChange} type="email" name="email" id="campoEmail" placeholder="entra email" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="campoPassword">Password</Label>
                        <Input onChange={this.handleInputChange} type="password" name="password" id="campoPassword" placeholder="entra password" />
                    </FormGroup>
                    <Button>Submit</Button>
                    {mensajeErrorPass}
                </Form>
                <h3>{this.state.email}</h3>
                <h3>{this.state.password}</h3>
            </Container>

        )
    }
}
