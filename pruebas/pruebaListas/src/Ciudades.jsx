import React from 'react';
import { CIUTATS } from './datos.js'

class Ciudades extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        // let i = 0;
        // let ciudadesLi = CIUTATS.map(el => <li key={i++}>{el}</li>);
        // let ciudadesLi = CIUTATS.map((el,indice) => <li key={indice}>{el}</li>);
        let ciudadesLi = CIUTATS
            .filter(el => (el.toLowerCase())[0] === 's')
            .map((el, indice) => <li key={indice}>{el}</li>);


        let filas = CIUTATS
            .filter(el => (el.toLowerCase())[0] === 's')
            .map((el, index) => <tr key={index}><td>{index}</td><td>{el}</td></tr>)
        return (
            <>
                <h1>Ciudades</h1>
                <table>
                    <thead>
                        <tr>
                            <th>Numero</th>
                            <th>Ciudades</th>
                        </tr>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                </table>
            </>
        )
    }
}

export default Ciudades;