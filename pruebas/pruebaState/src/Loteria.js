import React from 'react';
import './css/loteria.css';
import { Container, Button } from 'reactstrap';


export default class Loteria extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            numeros: [1, 2, 3],
            persona: { nombre: "xx", edad: 22 }
        }
        this.clicar = this.clicar.bind(this);
        this.guardar = this.guardar.bind(this);
        this.recuperar = this.recuperar.bind(this);
        this.borrar = this.borrar.bind(this);
    }

    guardar() {
        localStorage.setItem('loteria', JSON.stringify(this.state.numeros));
    }

    recuperar() {
        let numerosGuardados =  JSON.parse(localStorage.getItem('loteria'));
        this.setState({numeros: numerosGuardados})
    }

    borrar() {
        this.setState({ numeros: [] });
    }

    clicar() {
        let nuevoNumero = Math.floor(Math.random() * 9);

        //FUNCIONA PERO MAL let nuevaLista = this.state.numeros; //crea identificador que apounta a la misma lista

        let nuevaLista = [...this.state.numeros]; //crea copia
        nuevaLista.push(nuevoNumero);

        this.setState({ numeros: nuevaLista });

    }

    render() {

        let bolas = this.state.numeros.map((el, index) => <div key={index} className="bola">{el}</div>)

        return (
            <Container>
                <br />
                <Button onClick={this.guardar}>Guardar</Button>
                <Button onClick={this.recuperar}>Recuperar</Button>
                <Button onClick={this.borrar}>Borrar</Button>
                <br />
                <br />
                {bolas}
                <br />
                <Button onClick={this.clicar}>Bola</Button>
            </Container>
        )
    }
}