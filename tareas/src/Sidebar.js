import React from "react";
import { Input, Button } from "reactstrap";

export default class SideBar extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            texto: '',
            color: ''
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.nuevaTarea = this.nuevaTarea.bind(this);
    }

    handleInputChange(evento) {
        const target = evento.target;
        const value = (target.type === 'checkbox') ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    nuevaTarea() {
        let nombreTarea = this.props.texto;
        let colorTarea = this.props.color;

        

        console.log("creando tarea...");
    }

    render() {
        return (
            <>
                <h4>Nueva tarea</h4>
                Tarea
                <Input type="text" name="texto" onChange={this.handleInputChange} />
                <br />
                Color
                <Input type="text" name="color" onChange={this.handleInputChange} />
                <br />
                <Button onClick={this.nuevaTarea} >Añadir tarea</Button>
            </>
        )
    }
}