import React from "react";
import { Container, Row, Col } from "reactstrap";

import Header from './Header';
import SideBar from "./Sidebar";
import Content from './Content';

const TAREAS_DEMO = [
  {
    id: 1,
    texto: "Comprar mascarilla",
    color: "red"
  },
  {
    id: 2,
    texto: "Hacer ejercicio TAREAS",
    color: "green"
  },
  {
    id: 3,
    texto: "Jugar GTA",
    color: "orange"
  },
];

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      tareas: TAREAS_DEMO
    }
    this.nuevaTarea = this.nuevaTarea.bind(this);
    this.eliminaTarea = this.eliminaTarea.bind(this);
  }

  nuevaTarea(texto, color) {
    let nuevaTarea = this.nuevaTarea();

    let nuevaLista = [...TAREAS_DEMO];
    nuevaLista.push(nuevaTarea);
    
    this.setState({ tareas: nuevaLista });
  }

  eliminaTarea(id) {

  }

  render() {
    return (
      <>
        <Header />
        <Container>
          <Row>
            <Col xs="3" className="sidebar">
              <SideBar nuevaTarea={this.nuevaTarea} />
            </Col>
            <Col className="content">
              <Content tareas={this.state.tareas} eliminaTarea={this.eliminaTarea} />
            </Col>
          </Row>
        </Container>
      </>
    )
  }
}

export default App;