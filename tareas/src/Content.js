import React from "react";
import { Button } from "reactstrap";


function Content(props) {

    let tareas = props.tareas
        .map((el, index) =>
            <div className="tarea">
                <p>{el.texto}</p>
                <i onClick={() => props.eliminarTarea(1)} className="fa fa-trash fa-2x"aria-hidden="true" ></i>
{/*                 <Button onClick={() => props.eliminarTarea(1)}>Eliminar tarea</Button>
 */}            </div>
        )
    return (
        <>
            <h4>Contenido</h4>
            {tareas}
        </>
    )

}

export default Content;