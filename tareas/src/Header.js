import React from "react";
import { Container } from 'reactstrap';
import './css/app.css';


function Header() {

    return (
        <Container fluid className="header">
            <Container>
                <h1><i className="fa fa-address-book-o iconTarea" aria-hidden="true"></i>Tareas</h1>
            </Container>
        </Container>
    )
}
export default Header;