import React from 'react';
import { Container, Row } from 'reactstrap';

export default class Fotos extends React.Component {

    constructor(props) {
        super(props);

        this.state = ({
            imagen: "https://i.pinimg.com/originals/e8/5f/f7/e85ff715a3e09074f7691a8db9673264.png",
        })
        this.cambiarFoto = this.cambiarFoto.bind(this);
    }

    cambiarFoto() {
        if (this.state.imagen === "coche") {
            this.setState({
                imagen: "https://cdn.pixabay.com/photo/2017/12/21/12/58/png-car-3031752_960_720.png"
            })
        } else if (this.state.imagen === "moto") {
            this.setState({
                imagen: "https://4.bp.blogspot.com/-AFvbOjYr_JA/WVr5q4SMROI/AAAAAAAAECU/JTeb4c308x48LNp2QaDbrVgberloYG31ACK4BGAYYCw/s1600/Im%25C3%25A1genes%2BPng%2BDe%2BMotos.png"
            })
        } else if (this.state.imagen === "bus") {
            this.setState({
                imagen: "https://www.stickpng.com/assets/images/580b585b2edbce24c47b2bf6.png"
            })
        }
    }
    render() {
        return (
            <Container >
                <Row>
                    <select onChange={this.cambiarFoto}>
                        <option value="coche">Coche</option>
                        <option value="moto" selected >Moto</option>
                        <option value="bici" >Bici</option>
                        <option value="bus">Bus</option>
                    </select>
                </Row>
                <Row>
                    <img heigh="300" width="300" src={this.state.imagen} alt="vehiculo" />
                </Row>
            </Container>
        )
    }
}