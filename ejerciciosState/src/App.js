import React from "react";
import Tricolor from "./Tricolor";
import Fotos from './Fotos';

export default () => (
  <>
    <h1>Welcome ejercicios de State!</h1>
    <Tricolor />
    <br />
    <Fotos />
  </>
);
