import React from 'react';
import './css/tricolor.css'
import { Container } from 'reactstrap';


function Bola(props) {
    let color = "";
    switch (props.color) {
        case 0:
            color = "grey"
            break;
        case 1:
            color = "red"
            break;
        case 2:
            color = "blue"
            break;
        case 3:
            color = "green"
            break;
        default:
            break;
    }
    return (
        <div onClick={props.clicar2} className="circulo" style={{ backgroundColor: color }}></div>
    )
}


export default class Tricolor extends React.Component {

    constructor(props) {
        super(props);
        this.state = ({
            color: 0
        })

        this.cambiarColor = this.cambiarColor.bind(this);
    }

    cambiarColor() {
        console.log("He clickado");
        if (this.state.color === 3) {
            this.setState({ color: 0 });
        } else {
            this.setState({ color: this.state.color + 1 })
        }
    }

    render() {
        return (
            <Container>
                <Bola clicar2={this.cambiarColor} color={this.state.color} />
            </Container>


            /* <div onClick={this.cambiarColor} className="circulo"></div> */
        );
    }
}
