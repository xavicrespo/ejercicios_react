import React from 'react';


function Bombilla(props) {

    let imgOn = "http://media3.s-nbcnews.com/j/MSNBC/Components/Photo/_new/081209-light-bulb-03.grid-4x2.jpg";
    let imgOff = "https://www.industrytap.com/wp-content/uploads/2016/02/incandescent-e1456179151174.jpg";

    let imagen = (props.encendido) ? imgOn : imgOff;

    return(
         <img src={imagen} width="200px" />
         );
}

export default Bombilla;
