
import React from 'react';

/* import './css/lista.css'
 */
export default class Lista extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            nombre: ""
        }
        this.cambiaFiltro = this.cambiaFiltro.bind(this);
        this.clicado = this.clicado.bind(this);
    }

    cambiaFiltro(event){
        this.setState({
            nombre: event.target.value
        })
    }

    
    clicado(nombre){
        this.setState({nombre});
    }


    render(){
        let itemsFiltrados = this.props.items
                    .filter(item => this.state.nombre==="" || item.toLowerCase().indexOf(this.state.nombre.toLowerCase())>-1)
                    .map(item => <li key={item} onClick={()=>this.clicado(item)} >{item}</li>);
        return (
            <div className="lista">
            <input onChange={this.cambiaFiltro} name="nombre" value={this.state.nombre} />
            <ul>{itemsFiltrados}</ul>
            </div>
        );
    }

}