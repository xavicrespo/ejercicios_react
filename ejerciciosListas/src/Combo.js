import React from 'react';
import { Container, Dropdown, Row, Col, DropdownToggle, DropdownItem } from 'reactstrap';


export default class Combo extends React.Component {

    constructor(props) {
        super(props);
    }


    render() {
        let datos = this.props.datos;
        /*         let sinRepetidos = datos.filter((valorActual, indiceActual, lista) => {
                    //Podríamos omitir el return y hacerlo en una línea, pero se vería menos legible
                    return lista.findIndex(valorDelArreglo => JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)) === indiceActual
                }); */

        let listaComarcas = datos.map(elemento => <option>{elemento.comarca}</option>);

        let listaCiudadesSel = datos.map(elemento => <option>{elemento.municipi}</option>);

        return (
            <Container>
                <Row>
                    <Col>
                        <h3>Comarcas</h3>
                        <select name="comarcas">
                            {listaComarcas}
                        </select>
                    </Col>
                    <Col>
                        <h3>Ciudades</h3>
                        <select name="ciudades">
                            {listaCiudadesSel}
                        </select>
                    </Col>
                </Row>
            </Container>
        )
    }
}