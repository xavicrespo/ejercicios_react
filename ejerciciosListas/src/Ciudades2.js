import React from 'react';
import { Container, ListGroup, ListGroupItem, Row, Input } from 'reactstrap';

class Ciudades2 extends React.Component {

    constructor(props) {

        super(props);
        this.state = ({
            //listaCiudades: this.props.items,
            //listaCiudadesFiltradas: [],
            nombre: ""
        })

        this.cambiaFiltro = this.cambiaFiltro.bind(this);
        this.clicado = this.clicado.bind(this);
    }


    cambiaFiltro(event) {
        this.setState({
            nombre: event.target.value
        })
    }


    clicado(nombre) {
        this.setState({ nombre });
    }

    render() {
        let styleRow = {
            margin: "10px 0px"
        }
        let itemsFiltrados = this.props.items.
            filter(ciudad => ciudad.toLowerCase().includes(this.state.nombre))
            .map(item => <ListGroupItem key={item} onClick={() => this.clicado(item)}>{item}</ListGroupItem>)

        return (
            <Container>
                <h2>Ciudades</h2>
                <Row style={styleRow}>
                    <Input onChange={this.cambiaFiltro} name="nombre" value={this.state.nombre} type="text" placeholder="filtra por ciudades" />
                </Row>
                <Row style={styleRow}>
                    <ListGroup>
                        {itemsFiltrados}
                    </ListGroup>
                </Row>
            </Container>
        )
    }
}

export default Ciudades2;