import React from 'react';
import { CIUTATS_CAT_20K } from './datos.js'
import { Container, Alert, Row, Col, Table } from 'reactstrap';


class Ciudades extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {
        let ciudades = CIUTATS_CAT_20K.map((el) => {
            el.poblacio = el.poblacio + "";
            el.poblacio = el.poblacio.replace(/\./g, "") * 1;
            return el;
        });

        let ciudadesFiltradas = CIUTATS_CAT_20K
            .filter(el => el.poblacio > 100000)
            .sort((a, b) => a.poblacio < b.poblacio ? 1 : -1);

        let filas = ciudadesFiltradas
            .map((elemento, index) =>
                <tr key={index}>
                    <td>{elemento.municipi}</td>
                    <td>{elemento.poblacio}</td>
                    <td>{elemento.provincia}</td>
                </tr>);
        return (
            <Container>
                <Table>
                    <thead>
                        <tr>
                            <th>Municipio</th>
                            <th>Población</th>
                            <th>Província</th>
                        </tr>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                </Table>
            </Container>
        )
    }
}

export default Ciudades;