import React from "react";
import Gato from './Gato';

function App() {

  return (
    <>
      <h1>Ejercicio Gato</h1>
      <Gato ancho="300" alto="300" nombre="Garfield" />
    </>
  );

}

export default App;
