import React from 'react';
import './css/gato.css';


class Gato extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            fondo: "green"
        }

        this.cambiafondo = this.cambiafondo.bind(this);
    }

    cambiafondo() {
        let nuevoState = {
            fondo: "red"
        }

        this.setState(nuevoState);

    }
    render() {
        let urlGato = "http://placekitten.com/" + this.props.ancho + "/" + this.props.alto;
        let estilo = {
            backgroundColor: this.state.fondo
        };

        return (
            <>
                <div className="caja-gato" style={estilo}>
                    <img src={urlGato} alt="Mi gato" />
                    <p>{this.props.nombre}</p>
                    <br />
                    <button onClick={this.cambiafondo}>Cambiar fondo</button>
                </div>
            </>
        );
    }
}

export default Gato;



// function Gato(props) {
//     let urlGato = "http://placekitten.com/" + props.ancho + "/" + props.alto;
//     return (
//         <div className="caja-gato">
//             <img src={urlGato} alt="Mi gato" />
//             <p>{props.nombre}</p>
//         </div>
//     );
// }
